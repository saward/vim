nnoremap <SPACE> <Nop>
" let mapleader="\<Space>"

" Plugins
source $XDG_CONFIG_HOME/nvim/plugins.vim

" Mappings
source $XDG_CONFIG_HOME/nvim/mappings.vim

" Settings for particular files
source $XDG_CONFIG_HOME/nvim/filetypes.vim

" Per language settings
source $XDG_CONFIG_HOME/nvim/languages.vim

" General
source $XDG_CONFIG_HOME/nvim/general.vim

" Custom functions and such
source $XDG_CONFIG_HOME/nvim/custom.vim
