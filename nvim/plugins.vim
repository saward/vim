" Plugins

call plug#begin('~/.local/share/nvim/plugged')

" Official common configuration for LSP:
Plug 'neovim/nvim-lspconfig'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'crusoexia/vim-monokai'
Plug 'fatih/molokai'
Plug 'Shougo/unite.vim'
Plug 'Shougo/vimfiler'
Plug 'Shougo/denite.nvim'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'chrisbra/Colorizer'
Plug 'mcchrish/nnn.vim'

" =Language Plugins=
" Go Plugins

" GraphQL
Plug 'jparise/vim-graphql'

" Latex Plugins
Plug 'lervag/vimtex'

" PHP Plugins
Plug 'Chiel92/vim-autoformat'
Plug 'roxma/LanguageServer-php-neovim',  {'do': 'composer install && composer run-script parse-stubs'}

" OPA Rego
Plug 'tsandall/vim-rego'

" Dart/Flutter
Plug 'reisub0/hot-reload.vim'

" TypeScript Plugins
" Plug 'leafgarland/typescript-vim'
" Plug 'ryanolsonx/vim-lsp-typescript'
" Below seems to cause an error
" Plug 'mhartington/nvim-typescript', {'do': ':!install.sh \| UpdateRemotePlugins'}

" Dart
Plug 'dart-lang/dart-vim-plugin'

call plug#end()


" Specific plugin configurations:

source $XDG_CONFIG_HOME/nvim/plugins/fzf.vim
source $XDG_CONFIG_HOME/nvim/plugins/deoplete.vim
source $XDG_CONFIG_HOME/nvim/plugins/denite.vim
source $XDG_CONFIG_HOME/nvim/plugins/vimtex.vim
source $XDG_CONFIG_HOME/nvim/plugins/vim-autoformat.vim
source $XDG_CONFIG_HOME/nvim/plugins/dart-vim-plugin.vim

" LSP Configurations
source $XDG_CONFIG_HOME/nvim/lsp.vim

let g:rehash256 = 1
let g:molokai_original = 1
colorscheme molokai
