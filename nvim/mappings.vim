noremap <Leader>p :Files<CR>
nnoremap <C-k> :Files<CR>

" Buffer navigation
noremap <silent> [b :bprevious<CR>
noremap <silent> ]b :bnext<CR>
noremap <silent> [B :bfirst<CR>
noremap <silent> ]B :blast<CR>

com! FormatXML :%!python3 -c "import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())"
nnoremap = :FormatXML<Cr>

" Set Alt to be used for navigation, both from within terminal and outside (:h
" terminal-emulator-input)
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" Function Keys

map <F3> :VimFilerExplorer<CR>
" map <F3> :Defx -columns=icons:git:filename:type -split=vertical -winwidth=35 -toggle -direction=topleft<CR>

nnoremap <Leader>at :call FloatTerm()<CR>
