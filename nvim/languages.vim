au BufRead,BufNewFile *.graphql set filetype=graphql
autocmd Filetype markdown setlocal ts=2 sts=2 sw=2 sts=2 et
autocmd Filetype html setlocal ts=2 sts=2 sw=2 sts=2 et
autocmd Filetype css setlocal ts=2 sts=2 sw=2 sts=2 et
autocmd Filetype scss setlocal ts=2 sts=2 sw=2 sts=2 et
autocmd Filetype php setlocal ts=2 sts=2 sw=2 sts=2 et
autocmd Filetype php setlocal ts=2 sts=4 sw=2 sts=2 et
autocmd Filetype json setlocal ts=2 sts=4 sw=4 sts=2 et
autocmd Filetype rego setlocal ts=2 sts=4 sw=4 sts=2 et
autocmd Filetype graphql setlocal ts=2 sts=2 sw=2 sts=2 et
autocmd Filetype ruby setlocal ts=2 sts=2 sw=2 sts=2 et
autocmd Filetype sql setlocal ts=2 sw=2 et
autocmd Filetype vim setlocal ts=2 sw=2 et
autocmd BufNewFile,BufRead *.gotmpl set syntax=gotexttmpl
