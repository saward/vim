set number
set relativenumber

set tabstop=2
set shiftwidth=2

" Display tabs so that it's more obvious when I'm mixing tabs into a file that
" uses spaces
set list
" set listchars=tab:>-
set listchars=eol:↵,trail:~,tab:>-,nbsp:␣

" Mouse (requires xclip or xsel etc installed.  check :h clipboard)
" Can use 'y' without a register to yank selection and paste outside vim
set mouse=a

" Guicursor prevents font size changes
set guicursor=
set clipboard+=unnamedplus

set colorcolumn=80
highlight ColorColumn ctermbg=8
