VIM configuration settings.

# neovim

Configuration in nvim folder.  XDG_CONFIG_HOME can be specified directly.  Check 'man nvim' for details.

Use :CheckHealth to see the status of plugins.

## Configs

Configs are split into files.  Inside the 'plugins' folder is a per-plugin config file, to keep matters clean.

# plugins


